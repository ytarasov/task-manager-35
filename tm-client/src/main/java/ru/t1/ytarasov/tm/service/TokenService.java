package ru.t1.ytarasov.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.service.ITokenService;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}
