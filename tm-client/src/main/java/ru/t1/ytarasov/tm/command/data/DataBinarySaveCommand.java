package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataBinarySaveRequest;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.IOException;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-binary-save";

    @NotNull
    public static final String DESCRIPTION = "Save to binary file";

    @SneakyThrows
    @Override
    public void execute() throws AbstractException, IOException {
        System.out.println("[DATA BINARY SAVE]");
        getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
