package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataJsonFasterXmlLoadRequest;
import ru.t1.ytarasov.tm.dto.request.data.DataXmlFasterXmlLoadRequest;

public final class DataXmlFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-xml-fasterxml-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from xml by FasterXml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[XML FASTERXML LOAD]");
        getDomainEndpoint().loadDataXmlFasterXml(new DataXmlFasterXmlLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
