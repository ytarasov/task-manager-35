package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataYamlLoadRequest;

public final class DataYamlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-yaml-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from yaml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[YAML LOAD]");
        getDomainEndpoint().loadDataYaml(new DataYamlLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
