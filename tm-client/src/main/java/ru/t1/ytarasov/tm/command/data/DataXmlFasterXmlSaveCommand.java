package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataXmlFasterXmlSaveRequest;

public final class DataXmlFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-xml-fasterxml-save";

    @NotNull
    public static final String DESCRIPTION = "Saves to xml by FasterXml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[XML FASTERXML SAVE]");
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlFasterXmlSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
