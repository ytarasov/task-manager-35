package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataXmlJaxbLoadRequest;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.IOException;

public final class DataXmlJaxbLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-xml-jaxb-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from xml by JAXB";

    @SneakyThrows
    @Override
    public void execute() throws AbstractException, IOException, ClassNotFoundException {
        System.out.println("[XML JAXB LOAD]");
        @NotNull final DataXmlJaxbLoadRequest request = new DataXmlJaxbLoadRequest(getToken());
        getDomainEndpoint().loadDataXmlJaxb(request);
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
