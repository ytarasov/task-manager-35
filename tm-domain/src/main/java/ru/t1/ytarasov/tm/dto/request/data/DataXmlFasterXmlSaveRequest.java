package ru.t1.ytarasov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlFasterXmlSaveRequest extends AbstractDataRequest {

    public DataXmlFasterXmlSaveRequest(@Nullable String token) {
        super(token);
    }

}
