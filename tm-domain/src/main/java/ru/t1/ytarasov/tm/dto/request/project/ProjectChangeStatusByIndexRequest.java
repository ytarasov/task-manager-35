package ru.t1.ytarasov.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.AbstractUserRequest;
import ru.t1.ytarasov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    private int projectIndex;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable String token) {
        super(token);
    }

}
