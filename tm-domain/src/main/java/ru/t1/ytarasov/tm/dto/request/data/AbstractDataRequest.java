package ru.t1.ytarasov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public abstract class AbstractDataRequest extends AbstractUserRequest {

    public AbstractDataRequest(@Nullable String token) {
        super(token);
    }

}
