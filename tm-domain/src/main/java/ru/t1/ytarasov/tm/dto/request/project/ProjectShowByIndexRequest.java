package ru.t1.ytarasov.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIndexRequest extends AbstractUserRequest {

    private int projectIndex;

    public ProjectShowByIndexRequest(@Nullable String token) {
        super(token);
    }

}
