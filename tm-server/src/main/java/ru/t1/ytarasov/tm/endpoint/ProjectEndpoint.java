package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ytarasov.tm.api.service.IProjectService;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.dto.request.project.*;
import ru.t1.ytarasov.tm.dto.response.project.*;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = projectService.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Project> projects = projectService.findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectShowByIndexResponse showProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Project project = projectService.findOneByIndex(userId, projectIndex);
        return new ProjectShowByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Project project = projectService.changeProjectStatusById(userId, projectId, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Project project = projectService.changeProjectStatusByIndex(userId, projectIndex, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Project project = projectService.changeProjectStatusById(userId, projectId, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Project project =
                projectService.changeProjectStatusByIndex(userId, projectIndex, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = projectService.changeProjectStatusById(userId, projectId, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = projectService.changeProjectStatusByIndex(userId, projectIndex, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = projectService.updateById(userId, projectId, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = projectService.updateByIndex(userId, projectIndex, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Project project = projectTaskService.removeProjectById(userId, projectId);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) {
        @Nullable final Session session =check(request);
        @Nullable final String userId = session.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Project project = projectService.removeByIndex(userId, projectIndex);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectClearResponse clearProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @Nullable final Session session = check(request);
        @NotNull IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String userId = session.getUserId();
        projectTaskService.clearAllProjects(userId);
        return new ProjectClearResponse();
    }

}
