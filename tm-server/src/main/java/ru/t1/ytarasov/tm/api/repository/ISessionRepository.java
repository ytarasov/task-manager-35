package ru.t1.ytarasov.tm.api.repository;

import ru.t1.ytarasov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
