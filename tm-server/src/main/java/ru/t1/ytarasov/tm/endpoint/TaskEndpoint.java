package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.dto.request.task.*;
import ru.t1.ytarasov.tm.dto.response.task.*;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Task> tasks = taskService.findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) {
        check(request);
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String userId = request.getToken();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.bindTaskToProject(userId, taskId, projectId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) {
        check(request);
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String userId = request.getToken();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.unbindTaskFromProject(userId, taskId, projectId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskShowByIndexResponse showTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Task task = taskService.findOneByIndex(userId, taskIndex);
        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskShowByProjectIdResponse showTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByProjectIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> tasks = taskService.findAllTasksByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = taskService.changeTaskStatusById(userId, taskId, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskStartByIndexResponse startTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Task task = taskService.changeTaskStatusByIndex(userId, taskIndex, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = taskService.changeTaskStatusById(userId, taskId, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Task task = taskService.changeTaskStatusByIndex(userId, taskIndex, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = taskService.changeTaskStatusById(userId, taskId, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = taskService.changeTaskStatusByIndex(userId, taskIndex, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = taskService.updateById(userId, taskId, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        final int taskIndex = request.getTaskIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = taskService.updateByIndex(userId, taskIndex, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = taskService.removeById(userId, taskId);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    ) {
        @Nullable final String userId = request.getToken();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Task task = taskService.removeByIndex(userId, taskIndex);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

}
