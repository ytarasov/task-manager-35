package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private static final String NEW_TASK_NAME = "new task";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "new task";

    @NotNull
    private static final String UPDATE_TASK_NAME = "update task";

    @NotNull
    private static final String UPDATE_TASK_DESCRIPTION = "update task";

    @NotNull
    private static final String CHANGE_STATUS_NAME = "test change status";

    @NotNull
    private static final String REMOVE_TASK_NAME = "remove";

    @Before
    public void setup() {
        taskRepository.add(new Task("test task 1"));
        taskRepository.add(new Task("test task 2"));
        taskRepository.add(new Task("test task 3"));
    }

    @After
    public void tearDown() {
        taskRepository.clear();
    }

    @Test
    public void findAll() {
        final int expectedSize = taskService.getSize();
        @NotNull final List<Task> tasks = taskService.findAll();
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithComparator() {
        final int expectedSize = taskService.getSize();
        @Nullable final List<Task> tasks = taskService.findAll(Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(tasks);
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithSort() {
        final int expectedSize = taskService.getSize();
        @Nullable final List<Task> tasks = taskService.findAll(Sort.BY_STATUS);
        Assert.assertNotNull(tasks);
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(""));
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        taskService.add(task);
        @Nullable final Task task1 = taskService.findOneById(task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void findOneByIndex() throws AbstractException {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(500));
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final Task task = new Task();
        int i = 0;
        for (Task task1 : tasks) {
            if (i == 2) {
                task.setId(task1.getId());
                task.setName(task1.getName());
                break;
            }
            i++;
        }
        @Nullable final Task task1 = taskService.findOneByIndex(i);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void getSize() {
        @Nullable final List<Task> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int currentSize = taskService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void changeStatusById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, "", Status.IN_PROGRESS));
        @NotNull final Task task = new Task(CHANGE_STATUS_NAME);
        task.setUserId(userId);
        taskService.add(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, task.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById("", task.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusById(userId, task.getId(), null));
        @Nullable final Task task1 = taskService.changeTaskStatusById(userId, task.getId(), Status.COMPLETED);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(task1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void changeStatusByIndex() throws AbstractException {
        @NotNull final Task task = new Task(CHANGE_STATUS_NAME);
        task.setUserId(userId);
        taskService.add(task);
        final int index = taskService.getSize(userId) - 1;
        Assert.assertTrue(index >= 0);
        @NotNull final Task taskFoundByIndex = taskService.findAll().get(index);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex("", index, Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(null, index, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(userId, -5, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(userId, 50, Status.COMPLETED));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusByIndex(userId, index, null));
        @NotNull final Task task1 = taskService.changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        Assert.assertEquals(task1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById("", task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, null, UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, "", UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, task.getId(), null, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, task.getId(), "", UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, ""));
        @NotNull final Task task1 = taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void updateByIndex() throws AbstractException {
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        @NotNull final String nameNew = "test1";
        @NotNull final String descriptionNew = "description new";
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(null, 0, UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex("", 0, UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(userId, -5, UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(userId, 500, UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(userId, 0, null, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(userId, 0, "", UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateByIndex(userId, 0, UPDATE_TASK_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateByIndex(userId, 0, UPDATE_TASK_NAME, ""));
        @NotNull final Task task1 = taskService.updateByIndex(userId, 0, nameNew, descriptionNew);
        Assert.assertEquals(nameNew, task1.getName());
        Assert.assertEquals(descriptionNew, task1.getDescription());
    }

    @Test
    public void remove() throws AbstractException {
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.remove(null));
        @NotNull final Task task = new Task(REMOVE_TASK_NAME);
        taskService.add(task);
        taskService.remove(task);
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void removeById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(""));
        @NotNull final Task task = new Task(REMOVE_TASK_NAME);
        taskService.add(task);
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractException {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(-5));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(500));
        @NotNull final Task task = new Task(REMOVE_TASK_NAME);
        taskService.add(task);
        taskService.removeByIndex(3);
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void findAllWithUserId() throws AbstractException {
        @Nullable final String nullUserId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(nullUserId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(""));
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME);
        @Nullable final List<Task> tasks = taskService.findAll(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllWithUserIdAndComparator() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(null, Sort.BY_CREATED.getComparator()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll("", Sort.BY_CREATED.getComparator()));
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME);
        @Nullable final List<Task> tasks = taskService.findAll(userId, Sort.BY_CREATED.getComparator());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findOneByIdAndUserId() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(userId, ""));
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(null, task.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById("", task.getId()));
        @Nullable final Task task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getName(), task1.getName());
    }

    @Test
    public void findOneByIndexAndUserId() throws AbstractException{
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex(null, 3));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex("", 3));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(userId, -5));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(userId, 500));
        @Nullable final Task task1 = taskService.findOneByIndex(userId, 0);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getName(), task1.getName());
    }

    @Test
    public void set() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(new Task("test set 1"));
        tasks.add(new Task("test set 1"));
        taskService.set(tasks);
        Assert.assertEquals(tasks.size(), taskService.getSize());
    }

    @Test
    public void getSizeWithUserId() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(null));
        taskService.create(userId, NEW_TASK_NAME);
        final int sizeExpected = taskService.findAll(userId).size();
        final int sizeCurrent = taskService.getSize(userId);
        Assert.assertEquals(sizeExpected, sizeCurrent);
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        @NotNull final Project project = new Project("test project");
        project.setId(project.getId());
        project.setUserId(userId);
        projectRepository.add(project);
        @Nullable final Task task = taskService.create(userId, "test bind to project");
        Assert.assertNotNull(task);
        projectTaskService.bindTaskToProject(userId, task.getId(), project.getId());
        @Nullable final Task task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(project.getId(), task1.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        @NotNull final Project project = new Project("test project");
        project.setId(project.getId());
        project.setUserId(userId);
        projectRepository.add(project);
        @Nullable final Task task = taskService.create(userId, "test bind to project");
        Assert.assertNotNull(task);
        projectTaskService.bindTaskToProject(userId, task.getId(), project.getId());
        @Nullable final Task task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(project.getId(), task1.getProjectId());
    }

    @Test
    public void clear() {
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void clearWithUserId() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(""));
        taskService.add(userId, new Task("test clear 1"));
        taskService.add(userId, new Task("test clear 2"));
        taskService.add(userId, new Task("test clear 3"));
        taskService.clear(userId);
        Assert.assertEquals(0, taskService.getSize(userId));
        Assert.assertNotEquals(0, taskService.getSize());
    }

}
