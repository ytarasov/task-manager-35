package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private static final String NEW_TASK_NAME = "new task";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "new task";

    @NotNull
    private static final String REMOVE_TASK_NAME = "remove";

    @Before
    public void setUp() {
        @NotNull final Task task = new Task("test task 1");
        @NotNull final Task task1 = new Task("test task 2");
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        tasks.add(task1);
        taskRepository.add(tasks);
    }

    @After
    public void tearDown() {
        taskRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertEquals(2, taskRepository.getSize());
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        taskRepository.add(task);
        Assert.assertEquals(3, taskRepository.getSize());
    }

    @Test
    public void addList() {
        final int oldSize = taskRepository.getSize();
        @NotNull final Task task = new Task("test list tasks 1");
        @NotNull final Task task1 = new Task("test list tasks 2");
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        tasks.add(task1);
        taskRepository.add(tasks);
        final int expectedSize = tasks.size() + oldSize;
        final int currentSize = taskRepository.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAll() {
        Assert.assertEquals(2, taskRepository.findAll().size());
    }

    @Test
    public void findAllWithComparator() {
        final int repositorySize = taskRepository.getSize();
        @Nullable final List<Task> tasks = taskRepository.findAll(Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(tasks);
        final int sortedListSize = tasks.size();
        Assert.assertEquals(repositorySize, sortedListSize);
    }

    @Test
    public void existsById(){
        Assert.assertFalse(taskRepository.existsById("123"));
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        taskRepository.add(task);
        Assert.assertTrue(taskRepository.existsById(task.getId()));
    }

    @Test
    public void findById() {
        Assert.assertNull(taskRepository.findOneById(""));
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        @NotNull final String taskId = task.getId();
        taskRepository.add(task);
        @Nullable final Task task2 = taskRepository.findOneById(taskId);
        Assert.assertNotNull(task2);
        @NotNull final String taskId2 = task2.getId();
        Assert.assertEquals(taskId, taskId2);
    }

    @Test
    public void findOneByIndex() {
        @Nullable final List<Task> tasks = taskRepository.findAll();
        @Nullable final Task task = taskRepository.add(new Task(NEW_TASK_NAME));
        final int index = 2;
        @Nullable final Task task1 = taskRepository.findOneByIndex(index);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void remove() {
        @Nullable final Task task = taskRepository.add(new Task(REMOVE_TASK_NAME));
        @Nullable final Task task1 = taskRepository.remove(task);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void removeById() {
        @Nullable final Task task = taskRepository.add(new Task(REMOVE_TASK_NAME));
        @Nullable final Task task1 = taskRepository.removeById(task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void removeByIndex() {
        @Nullable final Task task = taskRepository.add(new Task(REMOVE_TASK_NAME));
        final int index = 2;
        @Nullable final Task task1 = taskRepository.removeByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void clear() {
        taskRepository.clear();
        final int foundSize = taskRepository.getSize();
        Assert.assertEquals(0, foundSize);
    }

    @Test
    public void addWithUserId() {
        @Nullable final Task task = taskRepository.add(userId, new Task(NEW_TASK_NAME));
        Assert.assertNotNull(task);
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void getSizeWithUserId() {
        taskRepository.add(userId, new Task(NEW_TASK_NAME));
        final int repositorySize = taskRepository.getSize();
        @Nullable final List<Task> tasks = taskRepository.findAll(userId);
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int foundSize = taskRepository.getSize(userId);
        Assert.assertEquals(expectedSize, foundSize);
        Assert.assertTrue(foundSize <= repositorySize);
    }

    @Test
    public void findAllWithUserId() {
        taskRepository.add(userId, new Task("task with user id 1"));
        taskRepository.add(userId, new Task("task with user id 2"));
        final int repositorySize = taskRepository.getSize();
        Assert.assertTrue(taskRepository.findAll("").isEmpty());
        @Nullable final List<Task> tasks = taskRepository.findAll(userId);
        Assert.assertNotNull(tasks);
        final int foundSize = tasks.size();
        Assert.assertTrue(foundSize <= repositorySize);
    }

    @Test
    public void findAllWithUserIdAndComparator() {
        taskRepository.add(userId, new Task(NEW_TASK_NAME));
        final int repositorySize = taskRepository.getSize(userId);
        @Nullable final List<Task> tasks = taskRepository.findAll(userId, Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(tasks);
        final int sortedListSize = tasks.size();
        Assert.assertEquals(repositorySize, sortedListSize);
    }

    @Test
    public void create() {
        final int sizeBefore = taskRepository.getSize();
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME);
        final int sizeExpected = sizeBefore + 1;
        final int sizeAfter = taskRepository.getSize();
        Assert.assertEquals(sizeExpected, sizeAfter);
        Assert.assertEquals(NEW_TASK_NAME, task.getName());
    }

    @Test
    public void createWithNameAndDescription() {
        final int sizeBefore = taskRepository.getSize();
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        final int sizeExpected = sizeBefore + 1;
        final int sizeAfter = taskRepository.getSize();
        Assert.assertEquals(sizeExpected, sizeAfter);
        Assert.assertEquals(NEW_TASK_NAME, task.getName());
        Assert.assertEquals(NEW_TASK_DESCRIPTION, task.getDescription());
    }

    @Test
    public void createWithNameAndStatus() {
        final int sizeBefore = taskRepository.getSize();
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME, Status.IN_PROGRESS);
        final int sizeExpected = sizeBefore + 1;
        final int sizeAfter = taskRepository.getSize();
        Assert.assertEquals(sizeExpected, sizeAfter);
        Assert.assertEquals(NEW_TASK_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void createWithNameAndDescriptionAndStatus() {
        final int sizeBefore = taskRepository.getSize();
        @Nullable final Task task = taskRepository.create(userId,NEW_TASK_NAME, NEW_TASK_DESCRIPTION, Status.IN_PROGRESS);
        final int sizeExpected = sizeBefore + 1;
        final int sizeAfter = taskRepository.getSize();
        Assert.assertEquals(sizeExpected, sizeAfter);
        Assert.assertEquals(NEW_TASK_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
        Assert.assertEquals(NEW_TASK_DESCRIPTION, task.getDescription());
    }

    @Test
    public void findOneByIdWithUserId() {
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME);
        @NotNull final String taskId = task.getId();
        @Nullable final Task task1 = taskRepository.findOneById(userId, taskId);
        Assert.assertNotNull(task1);
        @NotNull final String taskId1 = task1.getId();
        Assert.assertEquals(taskId, taskId1);
    }

    @Test
    public void findOneByIndexWithUserId() {
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME);
        @NotNull final String taskId = task.getId();
        @Nullable final Task task1 = taskRepository.findOneByIndex(userId, 0);
        Assert.assertNotNull(task1);
        @NotNull final String taskId1 = task1.getId();
        Assert.assertEquals(taskId, taskId1);
    }

    @Test
    public void findByProjectId() {
        @NotNull final Project project = new Project("project with task");
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        @NotNull final String taskId = task.getId();
        task.setUserId(userId);
        task.setProjectId(project.getId());
        taskRepository.add(task);
        @Nullable final List<Task> tasks = taskRepository.findAllTasksByProjectId(userId, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        boolean foundFlag = false;
        for (Task task1 : tasks) {
            @NotNull final String taskId1 = task1.getId();
            foundFlag = taskId1.equals(taskId);
            if (foundFlag) break;
        }
        Assert.assertTrue(foundFlag);
    }

    @Test
    public void removeWithUserId() {
        @Nullable final Task task = taskRepository.create(userId, REMOVE_TASK_NAME);
        @NotNull final String taskId = task.getId();
        @Nullable final Task task1 = taskRepository.remove(userId, task);
        Assert.assertNotNull(task1);
        @NotNull final String taskId1 = task1.getId();
        Assert.assertEquals(taskId, taskId1);
    }

    @Test
    public void removeByIdWithUserId() {
        @Nullable final Task task = taskRepository.create(userId, REMOVE_TASK_NAME);
        @NotNull final String taskId = task.getId();
        @Nullable final Task task1 = taskRepository.removeById(userId, taskId);
        Assert.assertNotNull(task1);
        @NotNull final String taskId1 = task1.getId();
        Assert.assertEquals(taskId, taskId1);
    }

    @Test
    public void removeByIndexWithUserId() {
        @Nullable final Task task = taskRepository.create(userId, REMOVE_TASK_NAME);
        @NotNull final String taskId = task.getId();
        @Nullable final Task task1 = taskRepository.removeByIndex(userId, 0);
        Assert.assertNotNull(task1);
        @NotNull final String taskId1 = task1.getId();
        Assert.assertEquals(taskId, taskId1);
    }

    @Test
    public void set() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(new Task("test set 1"));
        tasks.add(new Task("test set 1"));
        taskRepository.set(tasks);
        Assert.assertEquals(tasks.size(), taskRepository.getSize());
    }

    @Test
    public void clearWithUserId() {
        taskRepository.create(userId, "test clear with user id 1");
        taskRepository.create(userId, "test clear with user id 2");
        final int expectedSize = taskRepository.getSize() - 2;
        taskRepository.clear(userId);
        final int foundSize = taskRepository.getSize();
        Assert.assertEquals(expectedSize, foundSize);
        Assert.assertEquals(0, taskRepository.getSize(userId));
    }

}
