package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.IUserService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.exception.user.ExistsEmailException;
import ru.t1.ytarasov.tm.exception.user.ExistsLoginException;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;
import ru.t1.ytarasov.tm.repository.UserRepository;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);

    @Before
    public void setup() {
        createUsers();
    }

    @After
    public void tearDown() {
        userRepository.clear();
    }

    @Test
    public void add() {
        final int firstSize = userRepository.getSize();
        userService.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        final int expectedSize = firstSize + 1;
        final int currentSize = userRepository.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void addList() {
        final int firstSize = userService.getSize();
        @NotNull final List<User> users = new ArrayList<>();
        users.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        users.add(new User("test3", HashUtil.salt("test3", "452316", 7657)));
        users.add(new User("test4", HashUtil.salt("test4", "452316", 7657)));
        userService.add(users);
        final int expectedSize = firstSize + users.size();
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void existsById() {
        @NotNull final User user = userService.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        @NotNull final String id = user.getId();
        Assert.assertTrue(userService.existsById(id));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void getSize() {
        final int size1 = userService.findAll().size();
        final int size2 = userService.getSize();
        Assert.assertEquals(size1, size2);
    }

    @Test
    public void findOneById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(""));
        @NotNull final User user = userService.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        @NotNull final String id = user.getId();
        @Nullable final User user1 = userService.findOneById(id);
        Assert.assertNotNull(user1);
        Assert.assertEquals(id, user1.getId());
    }

    @Test
    public void findOneByIndex() throws AbstractException {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(-5));
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(100));
        final int index = 1;
        @Nullable final List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        int i = 0;
        @NotNull String expectedId = "";
        for (User user : users) {
            if (i == index) {
                expectedId += user.getId();
                break;
            }
            i++;
        }
        @Nullable final User user = userService.findOneByIndex(index);
        Assert.assertNotNull(user);
        @NotNull final String foundId = user.getId();
        Assert.assertEquals(expectedId, foundId);
    }

    @Test
    public void findByLogin() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        @Nullable final User user = userService.findByLogin("test");
        Assert.assertNotNull(user);
    }

    @Test
    public void registry() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, "test12", "test12@test.ru"));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "test", "test12@test.ru"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("test12", null, "test12@test.ru"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("test12", "", "test12@test.ru"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create("test12", "test", ""));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("test", "test12", "test12@test.ru"));
        Assert.assertThrows(ExistsEmailException.class, () -> userService.create("test12", "test", "test@test.com"));
        final int expectedSize = userService.getSize() + 1;
        @NotNull final User user = userService.create("test12", "test12", "test12@test.ru");
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void changePassword() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(null, "test2"));
        @Nullable final User user = userService.findByLogin("test");
        Assert.assertNotNull(user);
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), ""));
        @Nullable final User user1 = userService.setPassword(user.getId(), "test2");
        Assert.assertEquals(user.getId(), user1.getId());
    }

    @Test
    public void remove() throws AbstractException {
        Assert.assertThrows(ModelNotFoundException.class, () ->userService.remove(null));
        @NotNull final User user = userService.add(new User("trt", HashUtil.salt("trt", "452316", 7657)));
        final int expectedSize = userService.getSize() - 1;
        userService.remove(user);
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void removeById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(null));
        @NotNull final User user = userService.add(new User("trt", HashUtil.salt("trt", "452316", 7657)));
        final int expectedSize = userService.getSize() - 1;
        userService.removeById(user.getId());
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void removeByIndex() throws AbstractException {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(-5));
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(500));
        @NotNull final User user = userService.add(new User("trt", HashUtil.salt("trt", "452316", 7657)));
        final int expectedSize = userService.getSize() - 1;
        userService.removeByIndex(0);
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void set() {
        @NotNull final List<User> users = new ArrayList<>();
        final int sizeBefore = userService.getSize();
        users.add(new User("t1", HashUtil.salt("t1", "452316", 7657)));
        users.add(new User("t2", HashUtil.salt("t2", "452316", 7657)));
        users.add(new User("t3", HashUtil.salt("t3", "452316", 7657)));
        userService.set(users);
        final int currentSize = userService.getSize();
        Assert.assertNotEquals(sizeBefore, currentSize);
        Assert.assertEquals(users.size(), currentSize);
    }

    @Test
    public void clear() {
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    private void createUsers() {
        @Nullable final String passwordHash = HashUtil.salt("test", "452316", 7657);
        Assert.assertNotNull(passwordHash);
        @NotNull final User user = new User("test", passwordHash, "test@test.com");
        userService.add(user);
        @Nullable final String passwordHashAdmin = HashUtil.salt("admin", "452316", 7657);
        @NotNull final User user1 = new User("admin", passwordHashAdmin, Role.ADMIN);
        user1.setEmail("admin@test.com");
        userService.add(user1);
    }
}
