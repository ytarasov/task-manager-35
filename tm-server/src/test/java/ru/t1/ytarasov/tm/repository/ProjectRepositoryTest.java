package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String userId2 = UUID.randomUUID().toString();

    @NotNull
    private static final String NEW_PROJECT_NAME = "new project";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "new project";

    @NotNull
    private static final String REMOVE_PROJECT_NAME = "remove";

    private void createTestDataSet() {
        @NotNull List<Project> projects = new ArrayList<>();
        projects.add(new Project("test project 1", "test project one", Status.IN_PROGRESS));
        projects.add(new Project("test project 2", "test project two", Status.COMPLETED));
        projectRepository.add(projects);
    }

    private void clearTestDataSet() {
        projectRepository.clear();
    }

    @Before
    public void setUp() {
        createTestDataSet();
    }

    @After
    public void tearDown() {
        clearTestDataSet();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(2, projectRepository.getSize());
    }

    @Test
    public void getSize() {
        final int size1 = projectRepository.findAll().size();
        final int size2 = projectRepository.getSize();
        Assert.assertEquals(size1, size2);
    }

    @Test
    public void findAllWithComparator() {
        Assert.assertNotNull(projectRepository.findAll(Sort.BY_STATUS.getComparator()));
        final int projectListSize = projectRepository.getSize();
        final int projectSortedRepository = projectRepository.findAll(Sort.BY_STATUS.getComparator()).size();
        Assert.assertEquals(projectListSize, projectSortedRepository);
    }

    @Test
    public void clear() {
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void findOneById() {
        @NotNull final Project project = new Project(NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        projectRepository.add(project);
        Assert.assertNotNull(project.getId(), projectRepository.findOneById(project.getId()));
        Assert.assertEquals(project, projectRepository.findOneById(project.getId()));
        Project project1 = new Project("test id2", "test id2");
        Assert.assertNotEquals(project1, projectRepository.findOneById(project.getId()));
        Assert.assertNull(project1.getId(), projectRepository.findOneById(project1.getId()));
    }

    @Test
    public void existsById() {
        @NotNull final Project project = new Project("test exists", "test exists");
        projectRepository.add(project);
        Assert.assertEquals(project, projectRepository.findOneById(project.getId()));
    }

    @Test
    public void findOneByIndex() {
        @Nullable Project project = null;
        int index = -1;
        for (Project project1 : projectRepository.findAll()) {
            project = project1;
            index++;
            if (index == 0) break;
        }
        Assert.assertNotNull(projectRepository.findOneByIndex(index));
        Assert.assertEquals(project, projectRepository.findOneByIndex(index));
        Assert.assertNotEquals(project, projectRepository.findOneByIndex(1));
    }

    @Test
    public void remove() {
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectRepository.add(project);
        Assert.assertEquals(3, projectRepository.findAll().size());
        projectRepository.remove(project);
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @Test
    public void removeById() {
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        @NotNull final String projectId = project.getId();
        projectRepository.add(project);
        Assert.assertEquals(3, projectRepository.findAll().size());
        projectRepository.removeById(projectId);
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @Test
    public void removeByIndex() {
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectRepository.add(project);
        int index = 0;
        for (Project project1 : projectRepository.findAll()) {
            if (project1.getId().equals(project.getId())) break;
            index++;
        }
        projectRepository.removeByIndex(index);
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @Test
    public void addWithUserId() {
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectRepository.add(userId, project);
    }

    @Test
    public void create() {
        final int oldRepositorySize = projectRepository.getSize();
        @NotNull final Project createdProject = projectRepository.create(userId, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        final int expectedSize = oldRepositorySize + 1;
        final int newRepositorySize = projectRepository.getSize();
        Assert.assertEquals(userId, createdProject.getUserId());
        Assert.assertEquals(expectedSize, newRepositorySize);
        Assert.assertEquals(NEW_PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(NEW_PROJECT_DESCRIPTION, createdProject.getDescription());
    }

    @Test
    public void createWithNameAndStatus() {
        final int oldRepositorySize = projectRepository.getSize();
        @NotNull final Project createdProject = projectRepository.create(userId, NEW_PROJECT_NAME, Status.COMPLETED);
        final int expectedSize = oldRepositorySize + 1;
        final int newRepositorySize = projectRepository.getSize();
        Assert.assertEquals(userId, createdProject.getUserId());
        Assert.assertEquals(expectedSize, newRepositorySize);
        Assert.assertEquals(NEW_PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(Status.COMPLETED, createdProject.getStatus());
    }

    @Test
    public void createWithNameAndDescriptionStatus() {
        final int oldRepositorySize = projectRepository.getSize();
        @NotNull final Project createdProject = projectRepository.create(userId, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION, Status.COMPLETED);
        final int expectedSize = oldRepositorySize + 1;
        final int newRepositorySize = projectRepository.getSize();
        Assert.assertEquals(userId, createdProject.getUserId());
        Assert.assertEquals(expectedSize, newRepositorySize);
        Assert.assertEquals(NEW_PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(NEW_PROJECT_DESCRIPTION, createdProject.getDescription());
        Assert.assertEquals(Status.COMPLETED, createdProject.getStatus());
    }

    @Test
    public void findAllWithUserId() {
        projectRepository.create(userId, "test 1 with user Id");
        projectRepository.create(userId, "test 2 with user Id");
        Assert.assertTrue(projectRepository.findAll("").isEmpty());
        Assert.assertEquals(2, projectRepository.findAll(userId).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        @NotNull final Project project = projectRepository.create(userId, "test find by id with user");
        @Nullable final Project project2 = projectRepository.findOneById(userId, project.getId());
        @Nullable final Project project3 = projectRepository.findOneById(userId2, project.getId());
        Assert.assertNotNull(project2);
        Assert.assertEquals(project.getId(), project2.getId());
        Assert.assertNull(project3);
    }

    @Test
    public void findAllWithUserIdAndComparator() {
        projectRepository.create(userId, "test 1 with user Id");
        projectRepository.create(userId, "test 2 with user Id");
        Assert.assertTrue(projectRepository.findAll("", Sort.BY_STATUS.getComparator()).isEmpty());
        Assert.assertEquals(2, projectRepository.findAll(userId, Sort.BY_STATUS.getComparator()).size());
    }

    @Test
    public void findOneByIndexWithUserId() {
        projectRepository.create(userId, "test 1 with user Id");
        projectRepository.create(userId, "test 2 with user Id");
        projectRepository.create(userId, "test 3 with user Id");
        @Nullable final List<Project> projects = projectRepository.findAll(userId);
        final int index = 1;
        @NotNull String id = "";
        int i = 0;
        for (Project project : projects) {
            if (i == index) {
                id += project.getId();
                break;
            }
            i++;
        }
        @Nullable final Project project = projectRepository.findOneByIndex(userId, index);
        Assert.assertEquals(id, project.getId());
    }

    @Test
    public void getSizeWithUserId() {
        final int size1 = projectRepository.findAll(userId).size();
        final int size2 = projectRepository.getSize(userId);
        Assert.assertEquals(size1, size2);
    }

    @Test
    public void removeWithUserId() {
        final int sizeBefore = projectRepository.getSize();
        @Nullable final Project project = projectRepository.create(userId, REMOVE_PROJECT_NAME);
        projectRepository.remove(userId, project);
        final int sizeAfter = projectRepository.getSize();
        Assert.assertEquals(sizeBefore, sizeAfter);
    }

    @Test
    public void removeByIdWithUserId() {
        final int sizeBefore = projectRepository.getSize();
        @Nullable final Project project = projectRepository.create(userId, REMOVE_PROJECT_NAME);
        projectRepository.removeById(userId, project.getId());
        final int sizeAfter = projectRepository.getSize();
        Assert.assertEquals(sizeBefore, sizeAfter);
    }

    @Test
    public void removeByIndexWithUserId() {
        projectRepository.create(userId, "test 1 with user Id");
        projectRepository.create(userId, "test 2 with user Id");
        projectRepository.create(userId, "test 3 with user Id");
        @Nullable final List<Project> projects = projectRepository.findAll(userId);
        final int index = 1;
        @NotNull String id = "";
        int i = 0;
        for (Project project : projects) {
            if (i == index) {
                id += project.getId();
                break;
            }
            i++;
        }
        @Nullable final Project project = projectRepository.removeByIndex(userId, index);
        Assert.assertEquals(id, project.getId());
    }

    @Test
    public void set() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(new Project("test set 1"));
        projects.add(new Project("test set 1"));
        projectRepository.set(projects);
        Assert.assertEquals(projects.size(), projectRepository.getSize());
    }

    @Test
    public void clearWithUserId() {
        projectRepository.create(userId, "test 1 with user Id");
        projectRepository.create(userId, "test 2 with user Id");
        projectRepository.create(userId, "test 3 with user Id");
        final int expectedSize = projectRepository.getSize() - 3;
        projectRepository.clear(userId);
        final int currentSize = projectRepository.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

}
